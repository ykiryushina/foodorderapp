import { HeaderCartButton } from '../HeaderCartButton/HeaderCartButton';
import mealsImage from './../../../assets/meals.jpg';
import s from './Header.module.css';
export const Header = ({ onShowCart }) => {
    return (
        <>
            <header className={s.header}>
                <h1>ReactMeals</h1>
                <HeaderCartButton onClick={onShowCart} />
            </header>
            <div className={s['main-image']}>
                <img src={mealsImage} alt="A table full of delicious food" />
            </div>
        </>
    )
}