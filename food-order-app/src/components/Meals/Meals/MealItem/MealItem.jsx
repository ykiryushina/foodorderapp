import { useContext } from 'react';
import { CartContext } from '../../../../store/cart-context';
import s from './MealItem.module.css';
import { MealItemForm } from './MealItemForm/MealItemForm';

export const MealItem = ({ name, description, price, id }) => {
    const cartCtx = useContext(CartContext);

    const formattedPrice = `$${price.toFixed(2)}`

    const addToCartHandler = (amount) => {
        cartCtx.addItem({
            id: id,
            name: name,
            amount: amount,
            price: price
        })
    }

    return (
        <li className={s.meal}>
            <div>
                <h3>{name}</h3>
                <div className={s.description}>{description}</div>
                <div className={s.price}>{formattedPrice}</div>
            </div>
            <div>
                <MealItemForm id={id} onAddToCart={addToCartHandler} />
            </div>
        </li>
    )
}