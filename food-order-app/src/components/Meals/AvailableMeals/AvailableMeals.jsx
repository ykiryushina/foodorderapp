import { useEffect } from 'react';
import { useState } from 'react';
import { Card } from '../../UI/Card/Card';
import { MealItem } from '../Meals/MealItem/MealItem';
import { useHttp } from '../../hook/use-http';
import s from './AvailableMeals.module.css';

export const AvalableMeals = () => {
    const [meals, setMeals] = useState([]);

    const { sendRequest, isLoading, error } = useHttp();

    const transformFetchedData = (data) => {
        setMeals(Object.values(data)[0]);
    };

    useEffect(() => {
        sendRequest({ url: 'https://react-http-2dc58-default-rtdb.europe-west1.firebasedatabase.app/meals.json' }, transformFetchedData);
    }, []);

    const mealsList = meals.map(meal => (
        <MealItem
            id={meal.id}
            key={meal.id}
            name={meal.name}
            description={meal.description}
            price={meal.price}
        />
    ));
    return (
        <section className={s.meals}>
            <Card>
                <ul>
                    {mealsList}
                    {isLoading && <p>Loading...</p>}
                    {error && <p>{error}</p>}
                </ul>
            </Card>
        </section>
    )
}