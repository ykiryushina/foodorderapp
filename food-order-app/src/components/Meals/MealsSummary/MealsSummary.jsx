import s from './MealsSummary.module.css';

export const MealsSummary = () => {
    return (
        <section className={s.summary}>
            <h2>Delicious food</h2>
            <p>Choose your favorite meal from our broad selection
                of available meals and enjoya delicious lunch or
                dinner at home
            </p>
            <p>
                All our meals are cooked with high-quality ingredients,
                just-in-time and of course by experienced chefs!
            </p>
        </section>
    )
}