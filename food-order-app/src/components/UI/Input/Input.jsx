import { forwardRef } from 'react';
import s from './Input.module.css';

export const Input = forwardRef(({ label, input }, ref) => {
    return (
        <div className={s.input}>
            <label htmlFor={input.id}>{label}</label>
            <input {...input} ref={ref} />
        </div>
    )
}) 