import s from './Modal.module.css';
import ReactDOM from 'react-dom';

export const Backdrop = ({ onClose }) => {
    return <div className={s.backdrop} onClick={onClose}></div>
}

export const ModalOverlay = ({ children }) => {
    return <div className={s.modal}>
        <div className={s.content}>{children}</div>
    </div>
}

const portalElement = document.getElementById('overlays');

export const Modal = ({ children, onClose }) => {
    return (
        <>
            {ReactDOM.createPortal(<Backdrop onClose={onClose} />, portalElement)}
            {ReactDOM.createPortal(<ModalOverlay>{children}</ModalOverlay>, portalElement)}
        </>
    )
}