import { useInput } from '../../hook/use-input';
import { useHttp } from '../../hook/use-http';
import s from './OrderForm.module.css';
import { useContext } from 'react';
import { CartContext } from '../../../store/cart-context';

const isEmpty = value => value.trim() !== '';
const isLongEnough = value => value.length === 11;

export const OrderForm = ({ onClose, sendConfirmation }) => {
    const cartCtx = useContext(CartContext);
    const { sendRequest, isLoading, error } = useHttp();

    const {
        value: nameValue,
        isValid: nameIsValid,
        hasError: nameHasError,
        valueChangeHandler: nameChangeHandler,
        valueBlurHandler: nameBlurHandler,
        reset: nameReset
    } = useInput(isEmpty);
    const {
        value: cityValue,
        isValid: cityIsValid,
        hasError: cityHasError,
        valueChangeHandler: cityChangeHandler,
        valueBlurHandler: cityBlurHandler,
        reset: cityReset
    } = useInput(isEmpty);
    const {
        value: streetValue,
        isValid: streetIsValid,
        hasError: streetHasError,
        valueChangeHandler: streetChangeHandler,
        valueBlurHandler: streetBlurHandler,
        reset: streetReset
    } = useInput(isEmpty);
    const {
        value: houseValue,
        isValid: houseIsValid,
        hasError: houseHasError,
        valueChangeHandler: houseChangeHandler,
        valueBlurHandler: houseBlurHandler,
        reset: houseReset
    } = useInput(isEmpty);
    const {
        value: phoneValue,
        isValid: phoneIsValid,
        hasError: phoneHasError,
        valueChangeHandler: phoneChangeHandler,
        valueBlurHandler: phoneBlurHandler,
        reset: phoneReset
    } = useInput(isLongEnough);


    const submitHandler = (event) => {
        event.preventDefault();
        nameReset();
        cityReset();
        streetReset();
        houseReset();
        phoneReset();
    }

    let isFormValid = false;

    if (nameIsValid && cityIsValid && streetIsValid && houseIsValid && phoneIsValid) isFormValid = true;

    const nameClasses = nameHasError ? `${s.invalid}` : '';
    const cityClasses = cityHasError ? `${s.invalid}` : '';
    const streetClasses = streetHasError ? `${s.invalid}` : '';
    const houseClasses = houseHasError ? `${s.invalid}` : '';
    const phoneClasses = phoneHasError ? `${s.invalid}` : '';

    const orderData = {
        order: cartCtx.items,
        id: Date.now(),
        name: nameValue,
        city: cityValue,
        street: streetValue,
        house: houseValue,
        phone: phoneValue
    }

    const postOrder = () => {
        sendRequest({
            url: 'https://react-http-2dc58-default-rtdb.europe-west1.firebasedatabase.app/orders.json',
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: orderData
        })
        if (!isLoading && error === null) {
            sendConfirmation(true);
        }
    }

    return (
        <form className={s.form} onSubmit={submitHandler}>
            <div className={`${s.control} ${nameClasses}`}>
                <label htmlFor='name'>Name</label>
                <input
                    type='text'
                    id='name'
                    value={nameValue}
                    onChange={nameChangeHandler}
                    onBlur={nameBlurHandler}
                />
                {nameHasError && <p>Name must not be empty!</p>}
            </div>
            <div className={`${s.control} ${cityClasses}`}>
                <label htmlFor='city'>City</label>
                <input
                    type='text'
                    id='city'
                    value={cityValue}
                    onChange={cityChangeHandler}
                    onBlur={cityBlurHandler}
                />
                {cityHasError && <p>City must not be empty!</p>}
            </div>
            <div className={`${s.control} ${streetClasses}`}>
                <label htmlFor='street'>Street</label>
                <input
                    type='text'
                    id='street'
                    value={streetValue}
                    onChange={streetChangeHandler}
                    onBlur={streetBlurHandler}
                />
                {streetHasError && <p>Street must not be empty!</p>}
            </div>
            <div className={`${s.control} ${houseClasses}`}>
                <label htmlFor='house'>House</label>
                <input
                    type='text'
                    id='house'
                    value={houseValue}
                    onChange={houseChangeHandler}
                    onBlur={houseBlurHandler}
                />
                {houseHasError && <p>House must not be empty!</p>}
            </div>
            <div className={`${s.control} ${phoneClasses}`}>
                <label htmlFor='phone'>Phone</label>
                <input
                    type='number'
                    id='phone'
                    value={phoneValue}
                    onChange={phoneChangeHandler}
                    onBlur={phoneBlurHandler}
                />
                {phoneHasError && <p>Enter a valid phone number! (no need to enter a '+' sign)</p>}
            </div>
            <div className={s.actions}>
                <button onClick={onClose}>Cancel</button>
                <button disabled={!isFormValid} className={s.submit} onClick={postOrder} >Submit</button>
            </div>
        </form >
    );
}