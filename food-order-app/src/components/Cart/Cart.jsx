import { useState } from 'react';
import { useContext } from 'react';
import { CartContext } from '../../store/cart-context';
import { OrderForm } from '../Order/OrderForm/OrderForm';
import { Modal } from '../UI/Modal/Modal';
import s from './Cart.module.css';
import { CartItem } from './CartItem/CartItem';

export const Cart = ({ onClose }) => {
    const [isOrderButtonPressed, setIsOrderButtonPressed] = useState(false);
    const [isSendingConfirmed, setIsSendingConfirmed] = useState(false);
    const cartCtx = useContext(CartContext);
    const totalAmount = `$${cartCtx.totalAmount.toFixed(2)}`;
    const hasItems = cartCtx.items.length > 0;


    const cartItemRemoveHandler = (id) => {
        cartCtx.removeItem(id);
    }

    const cartItemAddHandler = (item) => {
        cartCtx.addItem({ ...item, amount: 1 });
    }

    const cartItems = <ul className={s['cart-items']}>
        {cartCtx.items.map((item) => (
            <CartItem
                key={item.id}
                name={item.name}
                amount={item.amount}
                price={item.price}
                onRemove={cartItemRemoveHandler.bind(null, item.id)}
                onAdd={cartItemAddHandler.bind(null, item)}
            />
        ))}
    </ul>;

    const orderBtnClickHandler = () => {
        setIsOrderButtonPressed(true);
    }

    const showConfirmation = (isConfirmed) => {
        setIsSendingConfirmed(isConfirmed);
    }

    const cartActions = <div className={s.actions}>
        <button className={s['button--alt']} onClick={onClose} >Close</button>
        {hasItems && <button className={s.buton} onClick={orderBtnClickHandler}>Order</button>}
    </div>

    return (
        <Modal onClose={onClose}>
            {!isSendingConfirmed && <div>
                {cartItems}
                <div className={s.total}>
                    <span>Total Amount</span>
                    <span>{totalAmount}</span>
                </div>
            </div>}
            {isSendingConfirmed && <p className={s.confirmation}>Your order is successfully sent!</p>}
            {!isSendingConfirmed && !isOrderButtonPressed && cartActions}
            {isOrderButtonPressed && !isSendingConfirmed && <OrderForm onClose={onClose} sendConfirmation={showConfirmation} />}
        </Modal >
    )
}