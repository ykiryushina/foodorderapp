import { useCallback, useState } from "react";

export const useHttp = () => {
    const [isLoading, setIsLoading] = useState(false);
    const [error, setError] = useState(null);

    const sendRequest = useCallback(async (requestConfig, transformData) => {
        const {
            url,
            method,
            headers,
            body
        } = requestConfig;

        setIsLoading(true);
        setError(null);
        try {
            const response = await fetch(url, {
                method: method ? method : 'GET',
                headers: headers ? headers : {},
                body: body ? JSON.stringify(body) : null
            })
            const data = await response.json();
            if (transformData) transformData(data);

        } catch (error) {
            setError(error)
        }
        setIsLoading(false);

    }, [])

    return {
        sendRequest,
        isLoading,
        error
    }
}